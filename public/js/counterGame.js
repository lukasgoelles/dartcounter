//score to check. default is 501
var startNumber = 501;
//doubleOut or singleOut. default is doubleOut
var doubleOut = true;
//name of the players
var players = new Array();
//current Player
var currentPlayer = 0;
//number of winners
var numberOfWinners = 0;
//number of exchange buttons
var numberOfExchangeButtons = 0;
//number of rounds
var playedRounds = 1;
//old score for undo
var oldScore = 0;
//old average for undo
var oldAvg = 0;
//times of undo
var timesOfUndo = 0;
//number of Players
var numberOfPlayers = 0;
//boolean whether the last player who clicked next has won
var hasLastWon = false;
//the name of the last player who won
var nameLastWon = "";
//the number of the player who won
var numberPlayerHasWon = null;
//the looser
var nameLost = "";
var avgLost = 0;
var oldScoreLast = 0;
var playedRoundsOverAll = [];
var scoreOverall = [];
var averageOverall = [];
var playedGames = 0;
var playerSort = [];
var oldScore = [];
var oldPlayedRoundsOverall = [];
var oldAverageOverall = [];
var lastscore = 0;
var sets = [];
var legs = [];
var changed = false;
var outs = []; 
var averagessaved = false;
var max = 0;
var scoreMatrix = [[]];

var possibleOuts = {
  40 : ['D20'],
  41 : ['9 D16','1 D20'],
  42 : ['10 D16','6 D18'],
  43 : ['11 D16','3 D20','19 D12'],
  44 : ['4 D20','12 D16'],
  45 : ['5 D20','13 D16','9 D18'],
  46 : ['6 D20','10 D18'],
  47 : ['15 D16','11 D18','7 D20'],
  48 : ['8 D20','16 D16'],
  49 : ['17 D16','9 D20'],
  50 : ['10 D20','18 D16','10 D20','BULL'],
  51 : ['11 D20','19 D16'],
  52 : ['12 D20','20 D16'],
  53 : ['13 D20','17 D18'],
  54 : ['14 D20','18 D18'],
  55 : ['15 D20','19 D18'],
  56 : ['16 D20','20 D18'],
  57 : ['17 D20','25 D16'],
  58 : ['18 D20','8 BULL'],
  59 : ['19 D20','25 D17'],
  60 : ['20 D20','10 BULL'],
  61 : ['25 D18','11 BULL','T11 D14'],
  62 : ['T10 D16','10 20 D16','12 BULL'],
  63 : ['T13 D12','13 BULL'],
  64 : ['T16 D8','16 16 D16','16 8 D20','14 BULL'],
  65 : ['25 D20','T19 D4','T15 D10'],
  66 : ['T10 D18','T16 D9'],
  67 : ['T17 D8','17 BULL'],
  68 : ['T20 D4','T16 D10'],
  69 : ['19 BULL','T11 D18','11 18 D20'],
  70 : ['T18 D8','20 BULL'],
  71 : ['T13 D16','25 6 D20','25 10 D18'],
  72 : ['T12 D18','T16 D12'],
  73 : ['T19 D8','T11 D20'],
  74 : ['T14 D16','T18 D10'],
  75 : ['T13 D18','25 BULL'],
  76 : ['T20 D8','T16 D14'],
  77 : ['T15 D16','T19 D10'],
  78 : ['T18 D12','T14 D18'],
  79 : ['T13 D20','T19 D11'],
  80 : ['T16 D16','T20 D10'],
  81 : ['T15 D18','T19 D12'],
  82 : ['T14 D20','BULL D16','25 17 D20'],
  83 : ['T17 D16','T11 BULL'],
  84 : ['T16 D18','16 T16 D10'],
  85 : ['T15 D20','T19 D14'],
  86 : ['T18 D16','BULL T18','25 11 BULL'],
  87 : ['T17 D18','T15 10 D16'],
  88 : ['T16 D20','T20 D14'],
  89 : ['T19 D16','T13 BULL'],
  90 : ['T18 D18','BULL D20'],
  91 : ['T17 D20','17 BULL D12'],
  92 : ['T20 D16','T16 11 D16'],
  93 : ['T19 D18','25 18 BULL'],
  94 : ['T18 D20','T16 6 D20'],
  95 : ['T19 D19','19 T20 D8','T15 BULL','15 T16 D16'],
  96 : ['T20 D18','20 T20 D8','T16 16 D16','T18 10 D16'],
  97 : ['T19 D20','19 T18 D12','T17 10 D18','17 T16 D16'],
  98 : ['T20 D19','20 T18 D12','T16 BULL','16 T16 D17'],
  99 : ['T19 10 D16','19 T16 D16','T20 7 D16','T17 8 D20','17 BULL D16'],
  100 : ['T20 D20','T16 12 D20','16 T16 D18','BULL BULL'],
  101 : ['T17 BULL','17 T16 D18','T13 12 BULL','13 T16 D20'],
  102 : ['T20 10 D16','20 BULL D16','T18 16 D16','18 T16 D18'],
  103 : ['T19 10 D16','19 T16 D18','T17 12 D20','17 T18 D16'],
  104 : ['T18 BULL','18 T18 D16','T16 16 D20'],
  105 : ['T20 13 D16','20 T15 D20','T19 8 D20','19 T18 D16'],
  106 : ['T20 10 D18','20 T18 D16','T18 12 D20','T16 18 D20','16 T18 D18'],
  107 : ['T19 BULL','19 T16 D20','T17 16 D20','17 T18 D18'],
  108 : ['T19 19 D16','18 T18 D18','T20 8 D20','20 T16 D20'],
  109 : ['T20 17 D16','20 T19 D16','T19 12 D20','19 T18 D18'],
  110 : ['T20 BULL','20 T18 D18','BULL 20 D20','25 T15 D20','15 T15 BULL'],
  111 : ['T20 19 D16','20 T17 D20','T19 14 BULL','17 T18 D20'],
  112 : ['T18 18 D20','T12 T20 D8','12 T20 D20'],
  113 : ['T20 13 D20','20 T19 D18','T19 16 D20','19 T18 D20','T17 12 BULL','17 T20 D18'],
  114 : ['T20 14 D20','T18 20 D20'],
  115 : ['19 T20 D18','T19 18 D20'],
  116 : ['T20 16 D20','T16 18 BULL'],
  117 : ['T20 18 D20','T17 16 BULL'],
  118 : ['T20 T19 D20','T18 T16 D8'],
  119 : ['19 T20 D20','T19 12 BULL'],
  120 : ['T20 20 D20','T19 13 BULL','T17 19 BULL'],
  121 : ['25 T20 D18','T19 T16 D8'],
  122 : ['T18 18 BULL','T20 12 BULL'],
  123 : ['T20 T13 D12','19 T18 BULL'],
  124 : ['T20 T16 D8','20 T18 BULL'],
  125 : ['T20 T19 D4','25 BULL BULL','25 T20 D20','T20 T11 D16'],
  126 : ['T19 19 BULL','T20 16 BULL','T18 T12 D18'],
  127 : ['T20 T17 D8','T20 T14 D16'],
  128 : ['T20 T20 D4','T20 18 BULL','T18 T18 D10'],
  129 : ['T19 T16 D12','T19 T12 D18','19 T20 BULL'],
  130 : ['T20 T18 D8','20 T20 BULL'],
  131 : ['T20 T13 D16','BULL T15 D18'],
  132 : ['T20 T16 D12','T17 T15 D18'],
  133 : ['T20 T19 D8','T17 BULL D16'],
  134 : ['T20 T14 D16','T18 T16 D16'],
  135 : ['T20 T13 D8','BULL T15 D20','25 T20 BULL'],
  136 : ['T20 T20 D8','T18 BULL D16'],
  137 : ['T20 T15 D16','T19 T16 D16'],
  138 : ['T20 T14 D18','T18 T16 D18'],
  139 : ['T20 T13 D20','T19 BULL D16','T17 T16 D20'],
  140 : ['T20 T20 D10','T18 T18 D16'],
  141 : ['T20 T15 D18','T19 T16 D18'],
  142 : ['T20 T14 D20','T18 T16 D20'],
  143 : ['T20 T17 D16','T19 T18 D16'],
  144 : ['T20 T20 D12','T18 T18 D18'],
  145 : ['T20 T15 D20','T19 T16 D20'],
  146 : ['T20 T18 D16','T19 T19 D16'],
  147 : ['T20 T17 D18','T19 T18 D18'],
  148 : ['T20 T20 D14','T20 T16 D20'],
  149 : ['T20 T19 D16','T17 T20 D19'],
  150 : ['T20 T18 D18','BULL BULL BULL'],
  151 : ['T20 T17 D20','T19 T20 D18'],
  152 : ['T20 T20 D16','T16 T18 BULL'],
  153 : ['T20 T19 D18'],
  154 : ['T20 T18 D20'],
  155 : ['T20 T19 D19'],
  156 : ['T20 T20 D18'],
  157 : ['T20 T19 D20'],
  158 : ['T20 T20 D19'],
  160 : ['T20 T20 D20'],
  161 : ['T20 T17 BULL'],
  164 : ['T20 T18 BULL'],
  167 : ['T20 T19 BULL'],
  170 : ['T20 T20 BULL']
};

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

/*
* Javascript for Counter
*/

//Pressing enter is the same as clicking "Next Player"
document.getElementById('score')
    .addEventListener('keyup', function (event) {
        event.preventDefault();
        if (event.keyCode == 13) {
            document.getElementById('next').click();
        }
    });
//Better support for touch devices as there is a numblock
$('.number').on('click', function() {
    //Append pressed number to score input
    var score = ($(this)[0].firstChild.nodeValue);
    document.getElementById('score').value+=score;
})
//remove last number from score input
$('#rmv').on('click', function() {
    var string = document.getElementById('score').value;
    document.getElementById('score').value=string.substring(0, string.length-1)
})
//reset all settings and switch to the settings-page to configure a new game
$('#quit').on('click', function() {
  //clear session Storage
  clearSessionStorage();
  //switch to the settings for a new game
  window.location.href = "./counterSettings.html";
})

$('#download').on('click', function() {
  var csv = [];
  for (ii = 0; ii< players.length; ii++){
    csv += players[ii];
    csv += ', Score left';
    if (ii< players.length-1){
      csv += ',';
    }
  }
  csv += "\n";
  scoreMatrix.forEach(function(row) {
          csv += row.join(',');
          csv += "\n";
  });

  var hiddenElement = document.createElement('a');
  hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
  hiddenElement.target = '_blank';
  hiddenElement.download = 'Score.csv';
  hiddenElement.click();
});


//restart game with the chosen settings
$('#restart').on('click', function() {
  //set currentPlayer = first Player
  if(changed){
    for(let i=0; i<players.length; i++){
      playerSort[i] = i; 
    }
    changed = false;
  }
  currentPlayer = 0;
  //set numberOfWinners = 0
  numberOfWinners = 0;
  //set numberOfExchangeButtons = 0
  numberOfExchangeButtons = 0;
  //set number of played rounds = 0
  playedRounds = 1;
  //reset for undo
  oldScore = 0;
  oldAvg = 0;
  timesOfUndo = 0;

  numberPlayerHasWon = null;
  hasLastWon = false;

  numberOfPlayers = players.length;

  //clear score input field
  document.getElementById('score').value = "";
  //if the game is restarted before it's finished clear the remaining player fields
  clearRemainingPlayerList();
  //create a new List of the players
  createPlayerList();
  //fill the list with data: names and score to start with
  fillNamesInList();
  createTableSetsLegs();
  createTableOuts();
  //return startnumber of each to set startnumber
  for(var i = 0; i < players.length; i++) {
    //remove latest scores
    //document.getElementsByName('avg')[i].innerHTML = "";
    //document.getElementsByName('avg')[i].style.display = "none";
    //set score to start with
    document.getElementsByName('playerScore')[i].innerHTML = startNumber;
    //deactivate all players
    //document.getElementsByName('player')[i].style.opacity = 0.5;
    //remove names from table
    document.getElementsByName('nameWinner')[i].innerHTML = "";
    //remove average from table
    document.getElementsByName('average')[i].innerHTML = "";
  }
  //show first player as active
  document.getElementsByName('playerScore')[0].style.fontSize = "35px";
  document.getElementsByName('playerScore')[0].style.fontWeight = "bold";
  //document.getElementsByName('player')[0].style.opacity = 1;
  //document.getElementsByName('avg')[0].style.display = "initial";
  //set cursor in input field
  document.getElementById('score').focus();
  //print current round
  document.getElementById('currentRound').innerText = playedRounds;
})

//handle the entered score and move on to next player
$('#next').on('click', function() {
  timesOfUndo = 0;
  hasLastWon = false;
  //get the score from input
  var score = document.getElementById('score').value;
  //if score input is empty set score to 0
  if(score == "") {
      score = 0;
  }
  //if score is not an integer print a message to the user
  if (!isInt(score)) {
      alert('Please enter a natural number.');
  //if score is higher than 180 print a message to the user
  } else if (score > 180) {
      alert('Please enter a score of 180 or less.');
  //If there are no more Players left
  } else if (document.getElementsByName('player').length === 0) {
      alert('Please start a new Game!');
  //if score is fine
  } else {
    try {
        oldScoreoverall = scoreOverall;
        oldAverageOverall = averageOverall;
        oldPlayedRoundsOverall = playedRoundsOverAll;
        lastscore = parseInt(score);

        //reset score input field
        document.getElementById('score').value = '';
        //get the currentScore of the player
        var currentScore = document.getElementsByName('playerScore')[currentPlayer].innerHTML;

        //get old avg and score for undo
        oldAvg = document.getElementsByName('avg')[currentPlayer].innerHTML;
        oldScore = currentScore;

        //get the new score
        var newScore = currentScore - score;
        if (currentPlayer == 0){
        max = maxVal(playedRoundsOverAll);
        function maxVal(playedRoundsOverAll){
          let max = 0;
          for (ii=0; ii<playedRoundsOverAll.length; ii++){
            if (playedRoundsOverAll[ii] > max){
              max = playedRoundsOverAll[ii];
            }
          }
          return max;
        }
        }
        try{
          scoreMatrix[max-1][2*playerSort[currentPlayer]] = score;
          scoreMatrix[max-1][2*playerSort[currentPlayer]+1] = newScore;
        }
        catch (e){
          scoreMatrix[max-1]= new Array(2*players.length);
          scoreMatrix[max-1][2*playerSort[currentPlayer]] = score;
          scoreMatrix[max-1][2*playerSort[currentPlayer]+1] = newScore;
        }
          
        if (newScore < 0) {
            var average = roundToTwo((startNumber - currentScore)/playedRounds);
            scoreOverall[playerSort[currentPlayer]] = parseInt(scoreOverall[playerSort[currentPlayer]]) + parseInt(score);
            averageOverall[playerSort[currentPlayer]] = roundToTwo(parseInt(scoreOverall[playerSort[currentPlayer]])/parseInt(playedRoundsOverAll[playerSort[currentPlayer]]));
            playedRoundsOverAll[playerSort[currentPlayer]] = playedRoundsOverAll[playerSort[currentPlayer]]+1;
            //document.getElementsByName('avg')[currentPlayer].innerHTML = average;
            document.getElementsByName('averagesr')[playerSort[currentPlayer]].innerHTML = average;
            appendSetList();
            nextPlayer();
        } else if (newScore === 1 && doubleOut) {
            var average = roundToTwo((startNumber - currentScore)/playedRounds);
            scoreOverall[playerSort[currentPlayer]] = parseInt(scoreOverall[playerSort[currentPlayer]]) + parseInt(score);
            averageOverall[playerSort[currentPlayer]] = roundToTwo(parseInt(scoreOverall[playerSort[currentPlayer]])/parseInt(playedRoundsOverAll[playerSort[currentPlayer]]));
            playedRoundsOverAll[playerSort[currentPlayer]] = playedRoundsOverAll[playerSort[currentPlayer]]+1;
            //document.getElementsByName('avg')[currentPlayer].innerHTML = average;
            document.getElementsByName('averagesr')[playerSort[currentPlayer]].innerHTML = average;
            appendSetList();
            nextPlayer();
        //if newScore is 0 the player is the Winner.
        } else if (newScore === 0) {
          for(let i=0; i<7; i++){
            document.getElementsByName("outs")[i].innerHTML="";
          }
          scoreOverall[playerSort[currentPlayer]] = parseInt(scoreOverall[playerSort[currentPlayer]]) + parseInt(score);
          averageOverall[playerSort[currentPlayer]] = roundToTwo(parseInt(scoreOverall[playerSort[currentPlayer]])/parseInt(playedRoundsOverAll[playerSort[currentPlayer]]));
          playedRoundsOverAll[playerSort[currentPlayer]] = playedRoundsOverAll[playerSort[currentPlayer]]+1;
          var avg = roundToTwo(startNumber/playedRounds);
          hasLastWon = true;
          legs[playerSort[currentPlayer]] = legs[playerSort[currentPlayer]] + 1;
          if(legs[playerSort[currentPlayer]] == 3){
            sets[playerSort[currentPlayer]] = sets[playerSort[currentPlayer]] + 1;
            for(let i=0; i<numberOfPlayers; i++){
              legs[i] = 0;
            }
          }
          nameLastWon = document.getElementsByName('playerName')[currentPlayer].innerHTML;
          numberPlayerHasWon = currentPlayer;
          var average = roundToTwo((startNumber)/playedRounds);
          document.getElementsByName('averagesr')[playerSort[currentPlayer]].innerHTML = average;
          //add the player to the table and remove player from the player list
          addPlayerToTable(avg);
          appendSetList();
          removePlayer();
          numberOfPlayers = numberOfPlayers - 1;
        //set the newScore as currentScore and add the score to latest scores. Then move on to next player
        } else {
            document.getElementsByName('playerScore')[currentPlayer].innerHTML = newScore;
            var average = roundToTwo((startNumber - newScore)/playedRounds);
            scoreOverall[playerSort[currentPlayer]] = parseInt(scoreOverall[playerSort[currentPlayer]]) + parseInt(score);
            averageOverall[playerSort[currentPlayer]] = roundToTwo(parseInt(scoreOverall[playerSort[currentPlayer]])/parseInt(playedRoundsOverAll[playerSort[currentPlayer]]));
            playedRoundsOverAll[playerSort[currentPlayer]] = playedRoundsOverAll[playerSort[currentPlayer]]+1;
            document.getElementsByName('averagesr')[playerSort[currentPlayer]].innerHTML = average;
            removeExchangeButtons();
            appendSetList();
            nextPlayer();
        }
    //catch all errors if something went wrong. Print a message to the user and set the first Player as current Player. Log the error.
    } catch (err) {
        alert('An error occured. Player 1 to throw now. Sorry for any inconvenience caused.');
        console.log(err);
        //sets opacity of current Player to 0.5
        document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "21px";
        document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "";
        document.getElementsByName('avg')[currentPlayer].style.display = "none";
        //set first player as active player
        currentPlayer = 0;
        document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "35px";
        document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "bold";
        //document.getElementsByName('player')[currentPlayer].style.opacity = 1;
        document.getElementsByName('avg')[currentPlayer].style.display = "initial";
    }
  }
})

$('#undo').on('click', function() {
  if (timesOfUndo != 0) {
    alert("Only one time 'Undo' allowed!");
  } else {
    if(!hasLastWon) {
      undo();
    } else {
      undoLastHasWon();
    }

    timesOfUndo = 1;

    if (!isMobile.any) {
      document.getElementById('score').focus();
    }

  }
});

/*
* Functions
*/

//parses an Item from session Storage
function parseFromSessionStorage(key) {
  return JSON.parse(window.sessionStorage.getItem(key));
}

//clears session Storage
function clearSessionStorage() {
  return window.sessionStorage.clear();
}

//onload function: prints the settings the player has chosen before
function init() {
  //if there are no parameters in sessionStorage redirect to settings page
  if(sessionStorage.length === 0) {
    //switch to the settings for a new game
    window.location.href = "./counterSettings.html";
  }


  //will be the String "double out" or "single out"
  var out;
  //get startNumber - this could be 301 or 501
  startNumber = parseFromSessionStorage('startNumber');
  //get doubleOut - this could be true (double) or false(single)
  doubleOut = parseFromSessionStorage('doubleOut');
  //get players - delivers an array of names
  players = parseFromSessionStorage('players');

  for(let i=0; i<players.length; i++){
    playedRoundsOverAll[i] = 1;
    scoreOverall[i] = 0;
    averageOverall[i] = 0;
    playerSort[i] = i;
    sets[i] = 0;
    legs[i] = 0;
  }

  //Check whether one of them is not set
  if(startNumber == null && doubleOut == null) {
    startNumber = 501;
    doubleOut = true;
  } else if (doubleOut == null) {
    doubleOut = true;
  } else if (startNumber == null) {
    startNumber = 501;
  }

  //if doubleOut is true set "Double Out" as String for var out else set "Single Out"
  if(doubleOut == true) {
    out = "Double Out";
  } else {
    out = "Single Out";
  }

  numberOfPlayers = players.length;

  //print the chosen settings
  document.getElementById('startnumber').innerText = startNumber;
  document.getElementById('out').innerText = out;
  //print current round
  document.getElementById('currentRound').innerText = playedRounds;

  //create the list for all of the players
  createPlayerList();
  //fill initial data in the created list
  fillNamesInList();
  //create table for final statistic
  createTable();
  //set first player as active
  //document.getElementsByName('player')[0].style.opacity = 1;
  document.getElementsByName('playerScore')[0].style.fontSize = "35px";
  document.getElementsByName('playerScore')[0].style.fontWeight = "bold";
  //set cursor in input field
  document.getElementById('score').focus();

  document.getElementById('restart').click();
}

//creates the list of all players
function createPlayerList() {
  //get div for players
  var parent = document.getElementById('players');
  for(var i = 0; i < players.length; i++){
    //create a new panel
    var div = document.createElement('div');
    div.className = "panel panel-default";
    div.setAttribute("name", "player");
    //create a new panel heading with name=playerName
    var playerName = document.createElement('div');
    playerName.className = "panel-heading";
    playerName.setAttribute("name", "playerName");
    //create a new panel body
    var panelBody = document.createElement('div');
    panelBody.className = "panel-body";
    //create current Score
    var playerScore = document.createElement('p');
    playerScore.setAttribute("name", "playerScore");
    //create latest Scores
    var averageText = document.createElement('p');
    averageText.innerHTML = "";
    var averageNumber = document.createElement('span');
    averageNumber.setAttribute("name", "avg");
    averageText.appendChild(averageNumber);

    //append current Score to panel body
    panelBody.appendChild(playerScore);
    //append latest scores to panel body
    panelBody.appendChild(averageText);
    //add panel heading and body to the created div
    div.appendChild(playerName);
    div.appendChild(panelBody);
    //add created div to parent div
    parent.appendChild(div);

    //add exchangeButtons if there are more than 1 players
    if((i + 1) !== players.length) {
      createExchangeButtons(parent);
    }
  }
}

//fills the names and the startNumber in the created player list
function fillNamesInList() {
  for(var i = 0; i < players.length; i++) {
    document.getElementsByName('playerName')[i].innerHTML = players[i];
    document.getElementsByName('playerScore')[i].innerHTML = startNumber;
    //document.getElementsByName('player')[i].style.opacity = 0.5;
    document.getElementsByName('playerScore')[i].style.fontSize = "21px";
    document.getElementsByName('playerScore')[i].style.fontWeight = "";
  }
}

//checks whether attribute is an integer
function isInt(value) {
    return !isNaN(value) &&
        parseInt(Number(value)) == value &&
        !isNaN(parseInt(value, 10)) && value >= 0;
}

//sets next Player
function nextPlayer() {
  //sets opacity of current Player to 0.5
  //document.getElementsByName('player')[currentPlayer].style.opacity = 0.5;
  document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "21px";
  document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "";
  //set currentPlayer to next player. If there is no other player left set currentPlayer to first player. Set opacity of the next Player to 1
  if(currentPlayer < document.getElementsByName('player').length - 1) {
    currentPlayer = currentPlayer + 1;
    if (possibleOuts[parseInt(document.getElementsByName('playerScore')[currentPlayer].textContent)]) {
      outs = possibleOuts[parseInt(document.getElementsByName('playerScore')[currentPlayer].textContent)];
    } else{
      outs = '';
    }
    appendSetListOuts();
    //document.getElementsByName('player')[currentPlayer].style.opacity = 1;
    document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "35px";
    document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "bold";
    //document.getElementsByName('avg')[currentPlayer].style.display = "initial";
  } else {
    currentPlayer = 0;
    playedRounds = playedRounds + 1;
    if (possibleOuts[parseInt(document.getElementsByName('playerScore')[currentPlayer].textContent)]) {
      outs = possibleOuts[parseInt(document.getElementsByName('playerScore')[currentPlayer].textContent)];
    } else{
      outs = '';
    }
    appendSetListOuts();
    document.getElementById('currentRound').innerText = playedRounds;
    //document.getElementsByName('player')[currentPlayer].style.opacity = 1;
    document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "35px";
    document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "bold";
    //document.getElementsByName('avg')[currentPlayer].style.display = "initial";
  }
}

function undo() {
  if (currentPlayer == 0) {
    if((playedRounds - 1) <= 0) {
      alert("Don't abuse the Undo Button!");
    } else {
      //reduce number of played Rounds
      playedRounds = playedRounds - 1;

      //sets opacity of current Player to 0.5
      //document.getElementsByName('player')[currentPlayer].style.opacity = 0.5;
      document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "21px";
      document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "";
      currentPlayer = (numberOfPlayers - 1);
      playedRoundsOverAll[playerSort[currentPlayer]] = playedRoundsOverAll[playerSort[currentPlayer]] - 1;
      scoreOverall[playerSort[currentPlayer]] = scoreOverall[playerSort[currentPlayer]] - lastscore;
      averageOverall[playerSort[currentPlayer]] = roundToTwo(parseInt(scoreOverall[playerSort[currentPlayer]])/parseInt(playedRoundsOverAll[playerSort[currentPlayer]]));

      document.getElementById('currentRound').innerText = playedRounds;
      //set old Player as active
      //document.getElementsByName('player')[currentPlayer].style.opacity = 1;
      document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "35px";
      document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "bold";
      //document.getElementsByName('avg')[currentPlayer].style.display = "initial";
      //reset old score of the player
      document.getElementsByName('playerScore')[currentPlayer].innerHTML = oldScore;
      //document.getElementsByName('avg')[currentPlayer].innerHTML = oldAvg;
      document.getElementsByName('averagesr')[currentPlayer].innerHTML = oldAvg;

      document.getElementById('score').focus();
    }

  } else {
    //sets opacity of current Player to 0.5
    //document.getElementsByName('player')[currentPlayer].style.opacity = 0.5;
    document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "21px";
    document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "";
    playedRounds = playedRounds - 1;

    currentPlayer = currentPlayer - 1;

      playedRoundsOverAll[playerSort[currentPlayer]] = playedRoundsOverAll[playerSort[currentPlayer]] - 1;
      scoreOverall[playerSort[currentPlayer]] = scoreOverall[playerSort[currentPlayer]] - lastscore;
      averageOverall[playerSort[currentPlayer]] = roundToTwo(parseInt(scoreOverall[playerSort[currentPlayer]])/parseInt(playedRoundsOverAll[playerSort[currentPlayer]]));

    //get the player before

    //set old Player as active
    d//ocument.getElementsByName('player')[currentPlayer].style.opacity = 1;
    document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "35px";
    document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "bold";
    //document.getElementsByName('avg')[currentPlayer].style.display = "initial";
    //reset old score of the player
    document.getElementsByName('playerScore')[currentPlayer].innerHTML = oldScore;
    //document.getElementsByName('avg')[currentPlayer].innerHTML = oldAvg;
    //document.getElementsByName('avgeragesr')[playerSort[currentPlayer]].innerHTML = oldAvg;

  }
}

function undoLastHasWon() {
  numberOfPlayers = numberOfPlayers + 1;
  //create old statistic for the player who won
  var parent = document.getElementById('players');
  var div = document.createElement('div');
  div.className = "panel panel-default";
  div.setAttribute("name", "player");
  //create a new panel heading with name=playerName
  var playerName = document.createElement('div');
  playerName.className = "panel-heading";
  playerName.setAttribute("name", "playerName");
  playerName.innerHTML = nameLastWon;
  //create a new panel body
  var panelBody = document.createElement('div');
  panelBody.className = "panel-body";
  //create current Score
  var playerScore = document.createElement('p');
  playerScore.setAttribute("name", "playerScore");
  playerScore.innerHTML = oldScore;
  //create latest Scores
  var averageText = document.createElement('p');
  averageText.innerHTML = "Average: ";
  var averageNumber = document.createElement('span');
  averageNumber.setAttribute("name", "avg");
  averageNumber.innerHTML = oldAvg;
  averageText.appendChild(averageNumber);
  //append current Score to panel body
  panelBody.appendChild(playerScore);
  //append latest scores to panel body
  panelBody.appendChild(averageText);
  //add panel heading and body to the created div
  div.appendChild(playerName);
  div.appendChild(panelBody);

  if (players.length == 1) {
    currentPlayer = 0;
    playedRounds = playedRounds - 1;
    document.getElementById('currentRound').innerText = playedRounds;

    //add created div to parent div
    parent.appendChild(div);

    //document.getElementsByName('player')[currentPlayer].style.opacity = 1;
    document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "35px";
    document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "bold";
    //document.getElementsByName('avg')[currentPlayer].style.display = "initial";

    //remove him from the winner table
    numberOfWinners = numberOfWinners - 1;
    document.getElementsByName('nameWinner')[numberOfWinners].innerHTML = "";
    //document.getElementsByName('average')[numberOfWinners].innerHTML = "";

  } else if (numberOfWinners == players.length) {


    var div2 = div.cloneNode(true);
    parent.appendChild(div);
    parent.appendChild(div2);

    if (numberPlayerHasWon == 0) {
      document.getElementsByName('playerName')[1].innerHTML = nameLost;
      document.getElementsByName('playerScore')[1].innerHTML = oldScoreLast;
      //document.getElementsByName('avg')[1].innerHTML = avgLost;
      document.getElementsByName('avgerages')[currentPlayer].innerHTML = avgLost;
      //document.getElementsByName('player')[1].style.opacity = 0.5;
      document.getElementsByName('playerScore')[1].style.fontSize = "21px";
      document.getElementsByName('playerScore')[1].style.fontWeight = "";
      currentPlayer = 0;
    } else {
      document.getElementsByName('playerName')[0].innerHTML = nameLost;
      document.getElementsByName('playerScore')[0].innerHTML = oldScoreLast;
      //document.getElementsByName('avg')[0].innerHTML = avgLost;
      //document.getElementsByName('avgeragesr')[playerSort[currentPlayer]].innerHTML = avgLost;
      //document.getElementsByName('player')[0].style.opacity = 0.5;
      document.getElementsByName('playerScore')[0].style.fontSize = "21px";
      document.getElementsByName('playerScore')[0].style.fontWeight = "";
      currentPlayer = 1;

      playedRounds = playedRounds - 1;
      document.getElementById('currentRound').innerText = playedRounds;
    }
    //document.getElementsByName('player')[currentPlayer].style.opacity = 1;
    document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "35px";
    document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "bold";

    numberOfWinners = numberOfWinners - 1;
    document.getElementsByName('nameWinner')[numberOfWinners].innerHTML = "";
    document.getElementsByName('average')[numberOfWinners].innerHTML = "";

    numberOfWinners = numberOfWinners - 1;
    document.getElementsByName('nameWinner')[numberOfWinners].innerHTML = "";
    document.getElementsByName('average')[numberOfWinners].innerHTML = "";


  } else {
      //document.getElementsByName('player')[currentPlayer].style.opacity = 0.5;
      document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "21px";
      document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "";

      if ((numberPlayerHasWon) === (numberOfPlayers - 1)) {
        currentPlayer = (numberOfPlayers - 1);

        playedRounds = playedRounds - 1;
        document.getElementById('currentRound').innerText = playedRounds;

        //add created div to parent div
        parent.appendChild(div);

        //document.getElementsByName('player')[currentPlayer].style.opacity = 1;
        document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "35px";
        document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "bold";
        //document.getElementsByName('avg')[currentPlayer].style.display = "initial";
      } else {
        parent.insertBefore(div, parent.childNodes[currentPlayer]);
        //document.getElementsByName('player')[currentPlayer].style.opacity = 1;
        document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "35px";
        document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "bold";
        //document.getElementsByName('avg')[currentPlayer].style.display = "initial";
      }

    //remove him from the winner table
    numberOfWinners = numberOfWinners - 1;
    document.getElementsByName('nameWinner')[numberOfWinners].innerHTML = "";
    document.getElementsByName('average')[numberOfWinners].innerHTML = "";
  }

}
//remove player from the list
function removePlayer() {
  //remove winning player from the list
  var elem = document.getElementsByName('player')[currentPlayer];
  elem.parentNode.removeChild(elem);
  //if it was the last player who won set the first player as next player to throw
  if(currentPlayer === document.getElementsByName('player').length) {
    currentPlayer = 0;
    playedRounds = playedRounds + 1;
    document.getElementById('currentRound').innerText = playedRounds;
  }
  //if there are more than one players left set the next player as active
  if(document.getElementsByName('player').length > 1) {
    //document.getElementsByName('player')[currentPlayer].style.opacity = 1;
    document.getElementsByName('playerScore')[currentPlayer].style.fontSize = "35px";
    document.getElementsByName('playerScore')[currentPlayer].style.fontWeight = "bold";
    document.getElementsByName('avg')[currentPlayer].style.display = "initial";
  }
  //if there is only one player left, add him to the table and remove the player from the list
  if(document.getElementsByName('player').length === 1) {
    var avg = document.getElementsByName('avg')[currentPlayer].innerHTML;
    oldScoreLast = document.getElementsByName('playerScore')[currentPlayer].innerHTML;
    nameLost = document.getElementsByName('playerName')[currentPlayer].innerHTML;
    avgLost = avg;
    addPlayerToTable(avg);
    elem = document.getElementsByName('player')[currentPlayer];
    elem.parentNode.removeChild(elem);
    numberOfPlayers = numberOfPlayers - 1;
  }
}
//adds current player to the table
function addPlayerToTable(avg) {
  //get name of the winner
  var winner = document.getElementsByName('playerName')[currentPlayer].innerHTML;
  //add this name to the table
  document.getElementsByName('nameWinner')[numberOfWinners].innerHTML = winner;
  //add average of the player to the table
  document.getElementsByName('average')[numberOfWinners].innerHTML = avg;
  //increase the numberOfWinners
  numberOfWinners = numberOfWinners + 1;
}
//create the table for statistic
function createTable() {
  var parent = document.getElementById('table');
  //add a row for each player
  for (var i = 1; i <= players.length; i++) {
    var tableRow = document.createElement('tr');

    var place = document.createElement('td');
    var text = document.createTextNode(i);
    place.appendChild(text);

    var name = document.createElement('td');
    name.setAttribute('name', 'nameWinner');

    var average = document.createElement('td');
    average.setAttribute('name', 'average');

    tableRow.appendChild(place);
    tableRow.appendChild(name);
    tableRow.appendChild(average);

    parent.appendChild(tableRow);
  }
}

function createTableSetsLegs() {
  var parent = document.getElementById('table2');
  for (let i = 0; i < players.length; i++) {
    var tableRow = document.createElement('tr');
    var name2 = document.createElement('td');
    name2.setAttribute('name', 'nameSetsLegs');

    var setlist = document.createElement('td');
    setlist.setAttribute('name', 'sets');

    var leglist = document.createElement('td');
    leglist.setAttribute('name', 'legs');

    var averagesr = document.createElement('td');
    averagesr.setAttribute('name', 'averagesr');

    var averages2 = document.createElement('td');
    averages2.setAttribute('name', 'averages2');

    tableRow.appendChild(name2);
    tableRow.appendChild(setlist);
    tableRow.appendChild(leglist);
    tableRow.appendChild(averagesr);
    tableRow.appendChild(averages2);

    parent.appendChild(tableRow);
  }
}

function createTableOuts() {
  var parent = document.getElementById('table3');
  for (let i = 0; i < 7; i++) {
    var tableRow = document.createElement('tr');
    var Outs = document.createElement('td');
    Outs.setAttribute('name', 'outs');
    tableRow.appendChild(Outs);

    parent.appendChild(tableRow);
  }
}

function appendSetListOuts() {
  for(let i=0; i<7; i++){
    document.getElementsByName("outs")[i].innerHTML="";
  }
  for(let i=0; i<outs.length; i++){
    document.getElementsByName("outs")[i].innerHTML=outs[i];
  }
}

function appendSetList() {
  for(let i = 0; i < players.length; i++){
    document.getElementsByName("nameSetsLegs")[i].innerHTML=players[i];
    document.getElementsByName("sets")[i].innerHTML=sets[i];
    document.getElementsByName("legs")[i].innerHTML=legs[i];
    document.getElementsByName("averages2")[i].innerHTML=averageOverall[i];
  }
}
//clear the remaining players from the list
function clearRemainingPlayerList() {
  //if there is a player left in the list
  if(document.contains(document.getElementsByName('player')[0])) {
    //remove all remaining players
    document.getElementById('players').innerHTML = "";
  }
}
//create exchange buttons
function createExchangeButtons(parent) {
  numberOfExchangeButtons = numberOfExchangeButtons + 1;
  //create a new row
  var row = document.createElement('div');
  row.setAttribute("class", "row");
  //create the button
  var exchangeButton = document.createElement('button');
  exchangeButton.setAttribute("type", "button");
  exchangeButton.setAttribute("class", "btn btn-default exchange");
  exchangeButton.setAttribute("name", numberOfExchangeButtons);
  //add event listener for clicking
  exchangeButton.addEventListener('click', function () {
    //get the name of the clicked button
    var number = this.name;
    //call exchangePlayer with the number of the exchangeButton
    exchangePlayer(number);
  })
  //create the exchange Sign
  var exchangeSign = document.createElement('span');
  exchangeSign.setAttribute("class", "glyphicon glyphicon-sort");
  exchangeSign.setAttribute("aria-hidden", "true");
  //create a new line
  var br = document.createElement('br');
  //append sign to button
  exchangeButton.appendChild(exchangeSign);
  //append button to row
  row.appendChild(exchangeButton);
  //append row to div
  parent.appendChild(row);
  //append new line to div
  parent.appendChild(br);
}

//remove all exchange buttons
function removeExchangeButtons() {
  //get the number of line breaks
  var numberBr = document.getElementsByTagName('br').length;
  //as there are 4 line breaks in HTML remove the 5th line break as long as there are any left
  for(var i = 4; i < numberBr; i++) {
    var br = document.getElementsByTagName('br')[4];
    br.parentNode.removeChild(br);
  }
  //get the number of exchange buttons
  var numberExchangeButtons = document.getElementsByClassName('exchange').length;
  //remove each exchange button
  for(var i = 0; i < numberExchangeButtons; i++) {
    var elem = document.getElementsByClassName('exchange')[0];
    elem.parentNode.removeChild(elem);
  }
}
//exchange two players
function exchangePlayer(n) {
  var playerOne = "";
  var playerTwo = "";
  //get name of the first player
  playerOne = document.getElementsByName('playerName')[(n - 1)].innerHTML;
  //get name of the second player
  playerTwo = document.getElementsByName('playerName')[n].innerHTML;
  //exchange names
  document.getElementsByName('playerName')[(n - 1)].innerHTML = playerTwo;
  document.getElementsByName('playerName')[n].innerHTML = playerOne;

  let player1 = n;
  let player2 = n-1;
  //console.log(player1);
  //console.log(player2);
  let oldvalue1 = playerSort[n];
  let oldvalue2 = playerSort[n-1];
  playerSort[n] = oldvalue2;
  playerSort[n-1] = oldvalue1;
  changed = true;
  //set cursor in input field
  document.getElementById('score').focus();
}

function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
}
